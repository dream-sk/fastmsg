import com.david.fastmsg.Constant.Constant;
import com.david.fastmsg.utils.Utils;
import com.sun.image.codec.jpeg.JPEGCodec;
import com.sun.image.codec.jpeg.JPEGImageEncoder;
import org.junit.Test;

import javax.imageio.ImageIO;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Date;
import java.util.regex.Pattern;

/**
 * @author SE0090
 * @date 2021/9/9
 */
public class UtilsTest {

//    @Test
//    public void testGetUserCode() {
//        for(int i=0; i < 2000; i++) {
//            System.out.println(Utils.getUserCode());
//        }
////        ;
//    }

    @Test
    public void tets() {
        boolean isMatch = Pattern.matches(Constant.imgPattern, ".jpg");
        System.out.println(isMatch);
    }


    @Test
    public void test11() {
        System.out.println(Utils.convertDate(new Date()));
    }

    @Test
    public void test01(){
        int width = 40;
        int height = 40;
        String s = "戚";

        File file = new File("D:/picpath.jpg");

        //创建一个画布
        BufferedImage bi = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        //获取画布的画笔
        Graphics2D g2 = (Graphics2D)bi.getGraphics();

        //开始绘图
        g2.setBackground(Color.WHITE);
        g2.clearRect(0, 0, width, height);
        g2.setPaint(Color.BLACK);
        g2.setFont(new Font("宋体", Font.BOLD, 25));
        //绘制字符串
        g2.drawString(s, 8, 30);

        try {
            //将生成的图片保存为jpg格式的文件。ImageIO支持jpg、png、gif等格式
            ImageIO.write(bi, "jpg", file);
        } catch (IOException e) {
            System.out.println("生成图片出错........");
            e.printStackTrace();
        }

    }

    @Test
    public void test04() {
        try {
            InputStream imagein1 = new FileInputStream("D:\\code\\fastmsg\\88581978_负责人.jpg");
            InputStream imagein2 = new FileInputStream("D:\\code\\fastmsg\\88581978_负责人.jpg");
            InputStream imagein3 = new FileInputStream("D:\\code\\fastmsg\\88595988_林露璐.jpg");
            BufferedImage bg_image = new BufferedImage(192, 144, BufferedImage.TYPE_INT_BGR);
            BufferedImage image1 = ImageIO.read(imagein1);
            BufferedImage image2 = ImageIO.read(imagein2);
            BufferedImage image3 = ImageIO.read(imagein3);
            Graphics g = bg_image.getGraphics();
            g.drawImage(image1, 0, 48, 48, 48,null);
            g.drawImage(image2, 48, 48, 48, 48,null);
            g.drawImage(image3, 96, 48, 48, 48,null);
            OutputStream outImage = new FileOutputStream("D:/playitemid_1_2.bmp");
            JPEGImageEncoder enc = JPEGCodec.createJPEGEncoder(outImage);
            enc.encode(bg_image);
            imagein1.close();
            imagein2.close();
            imagein3.close();
            outImage.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Test
    public void test02(){
        int width = 40;
        int height = 40;
        String s = "戚";

        File file = new File("D:/picpath.jpg");

        //创建一个画布
        BufferedImage bi = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        //获取画布的画笔
        Graphics2D g2 = (Graphics2D)bi.getGraphics();

        //开始绘图
        g2.setBackground(Color.WHITE);
        g2.clearRect(0, 0, width, height);
        g2.setPaint(Color.BLACK);
        g2.setFont(new Font("宋体", Font.BOLD, 25));
        //绘制字符串
        g2.drawString(s, 8, 30);

        try {
            //将生成的图片保存为jpg格式的文件。ImageIO支持jpg、png、gif等格式
            ImageIO.write(bi, "jpg", file);
        } catch (IOException e) {
            System.out.println("生成图片出错........");
            e.printStackTrace();
        }

    }


    public BufferedImage modifyImagetogeter(BufferedImage[] image) {
        //创建一个304*304的图片
        BufferedImage tag = new BufferedImage(304,304,BufferedImage.TYPE_INT_RGB);
        try {
            Graphics2D graphics = tag.createGraphics();
            //设置颜色为218，223，224
            graphics.setColor(new Color(218,223,224));
            //填充颜色
            graphics.fillRect(0, 0, 304, 304);
            int count = image.length;
            //根据不同的合成图片数量设置图片放置位置
            if(count == 1){
                int startX = 108;
                int startY = 108;
                BufferedImage b = image[0];
                graphics.drawImage(b, startX, startY, b.getWidth(), b.getHeight(), null);
            }else if(count == 2){
                int startX = 60;
                int startY = 108;
                BufferedImage b1 = image[0];
                graphics.drawImage(b1, startX, startY, b1.getWidth(), b1.getHeight(), null);
                BufferedImage b2 = image[1];
                startX = startX + b1.getWidth()+8;
                graphics.drawImage(b2, startX, startY, b2.getWidth(), b2.getHeight(), null);
            }else if(count == 3){
                int startX = 108;
                int startY = 60;
                BufferedImage b1 = image[0];
                graphics.drawImage(b1, startX, startY, b1.getWidth(), b1.getHeight(), null);
                BufferedImage b2 = image[1];
                startX = 60;
                startY = 60 + b1.getHeight() + 8;
                graphics.drawImage(b2, startX, startY, b2.getWidth(), b2.getHeight(), null);
                BufferedImage b3 = image[2];
                startX = startX + b2.getWidth()+8;
                graphics.drawImage(b3, startX, startY, b3.getWidth(), b3.getHeight(), null);
            }else if(count == 4){
                int startX = 60;
                int startY = 60;
                BufferedImage b1 = image[0];
                graphics.drawImage(b1, startX, startY, b1.getWidth(), b1.getHeight(), null);
                BufferedImage b2 = image[1];
                startX = 60 + b1.getWidth() + 8;
                graphics.drawImage(b2, startX, startY, b2.getWidth(), b2.getHeight(), null);
                BufferedImage b3 = image[2];
                startX = 60;
                startY = 60 + b2.getHeight() + 8;
                graphics.drawImage(b3, startX, startY, b3.getWidth(), b3.getHeight(), null);
                BufferedImage b4 = image[3];
                startX = 60 + b3.getWidth() + 8;
                graphics.drawImage(b4, startX, startY, b4.getWidth(), b4.getHeight(), null);
            }else if(count == 5){
                int startX = 60;
                int startY = 60;
                BufferedImage b1 = image[0];
                graphics.drawImage(b1, startX, startY, b1.getWidth(), b1.getHeight(), null);
                BufferedImage b2 = image[1];
                startX = startX + b1.getWidth()+8;
                graphics.drawImage(b2, startX, startY, b2.getWidth(), b2.getHeight(), null);
                startX = 12;
                startY = 12 + startY + b2.getHeight();
                for(int i = 2;i<count;i++){
                    BufferedImage b = image[i];
                    graphics.drawImage(b, startX, startY, b.getWidth(), b.getHeight(), null);
                    startX = startX + b.getWidth() + 8;
                }
            }else if(count == 6){
                int startX = 12;
                int startY = 60;
                for(int i = 0;i<count;i++){
                    BufferedImage b = image[i];
                    graphics.drawImage(b, startX, startY, b.getWidth(), b.getHeight(), null);
                    startX = startX + b.getWidth() + 8;
                    if((i+1)%3 == 0){
                        startY = startY + b.getHeight() + 8;
                        startX = 12;
                    }
                }
            }else if(count == 7){
                int startX = 108;
                int startY = 12;
                BufferedImage b = image[0];
                graphics.drawImage(b, startX, startY, b.getWidth(), b.getHeight(), null);
                startX = 12;
                startY = startY + 8 + b.getHeight();
                for(int i = 1;i<count;i++){
                    b = image[i];
                    graphics.drawImage(b, startX, startY, b.getWidth(), b.getHeight(), null);
                    startX = startX + b.getWidth() + 8;
                    if(i%3 == 0){
                        startY = startY + b.getHeight() + 8;
                        startX = 12;
                    }
                }
            }else if(count == 8){
                int startX = 60;
                int startY = 12;
                BufferedImage b1 = image[0];
                graphics.drawImage(b1, startX, startY, b1.getWidth(), b1.getHeight(), null);
                BufferedImage b2 = image[1];
                startX = startX + b1.getWidth()+8;
                graphics.drawImage(b2, startX, startY, b2.getWidth(), b2.getHeight(), null);
                startX = 12;
                startY = 12 + b2.getHeight() + 8;
                for(int i = 2;i<count;i++){
                    BufferedImage b = image[i];
                    graphics.drawImage(b, startX, startY, b.getWidth(), b.getHeight(), null);
                    startX = startX + b.getWidth() + 8;
                    if(i == 4){
                        startY = startY + b.getHeight() + 8;
                        startX = 12;
                    }
                }
            }else if(count == 9){
                int startX = 12;
                int startY = 12;
                for(int i = 0;i<count;i++){
                    BufferedImage b = image[i];
                    graphics.drawImage(b, startX, startY, b.getWidth(), b.getHeight(), null);
                    startX = startX + b.getWidth() + 8;
                    if((i+1)%3 == 0){
                        startY = startY + b.getHeight() + 8;
                        startX = 12;
                    }
                }
            }
            graphics.dispose();
        } catch (Exception e) {
//            logger.error("推送同比压缩图片出错{}",e);
        }

        return tag;
    }

    public BufferedImage loadImageLocal(String imgName) {
        try {
            return ImageIO.read(new File(imgName));
//        	return ImageIO.read(new URL(imgName));
        } catch (IOException e) {
//            logger.error("推送同比压缩图片出错{}",e);
        }
        return null;
    }

    public void writeImageLocal(String newImage, BufferedImage img) {
        if (newImage != null && img != null) {
            try {
                File outputfile = new File(newImage);
                ImageIO.write(img, "jpg", outputfile);
            } catch (IOException e) {
//                logger.error("推送同比压缩图片出错{}",e);
            }
        }
    }


    private BufferedImage[] handleLarge(Integer width,Integer height,BufferedImage[] image) {
        BufferedImage[] b = new BufferedImage[image.length];
        for (int i = 0; i < image.length; i++) {
            BufferedImage sourceImage = image[i];
            try {
                b[i] = zoom2(width, height, sourceImage);
            } catch (Exception e) {
//                logger.error("推送同比压缩图片出错{}",e);
            }
        }
        return b;
    }

    @Test
    public void main() {
//		BufferedImage b1 = loadImageLocal("C:\\Users\\Admin\\Documents\\WeChat Files\\a010662716\\FileStorage\\File\\2020-04\\头像\\");
        try {
            //用于合成的图片数量
            int count = 9;
            BufferedImage[] image =new BufferedImage[count];
            for(int i =0;i<count;i++){
                image[i]=loadImageLocal("D:\\"+(i+1)+".jpg");
            }
            writeImageLocal("D:\\out.png", modifyImagetogeter(handleLarge(88,88,image)));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public BufferedImage zoom2(int width,int height,BufferedImage sourceImage) throws Exception {

        if( sourceImage == null ){
            return sourceImage;
        }
        // 计算x轴y轴缩放比例--如需等比例缩放，在调用之前确保參数width和height是等比例变化的
        double ratiox  = (new Integer(width)).doubleValue()/ sourceImage.getWidth();
        double ratioy  = (new Integer(height)).doubleValue()/ sourceImage.getHeight();
        AffineTransformOp op = new AffineTransformOp(AffineTransform.getScaleInstance(ratiox, ratioy), null);
        BufferedImage bufImg = op.filter(sourceImage, null);
        return bufImg;
    }


    @Test
    public void test03() {
        Utils.generateHead("戚郑伟");
    }
}
