package com.david.fastmsg.Constant;

/**
 * @author qzw
 * @date 2021/9/7
 */
public class Constant {
    public static final String SERVER = "文件传输助手";
    public static final String USER_NOT_ONLINE = "当前用户不在线";
    // 好友邀请
    public static final String FRIEND_REQUEST = "1";
    // 好用同意
    public static final String FRIEND_AGREE = "2";
    public static final String HTTP = "http://";

    public static final String imgPattern = ".(JPEG|jpeg|JPG|jpg|png|PNG)";
    public static final int NOT_IS_GROUP = 0;
    public static final int IS_GROUP = 1;
}
