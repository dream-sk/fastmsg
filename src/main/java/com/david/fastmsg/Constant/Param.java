package com.david.fastmsg.Constant;

/**
 * @author qzw
 * @date 2021/9/6
 */
public class Param {
    public static final String RESP_CODE = "respCode";
    public static final String RESP_MSG = "respMsg";
    public static final String USER_ID = "userId";
    public static final String USER_NAME = "userName";
    public static final String SERVER_IP = "serverIP";
    public static final String URL = "url";
    public static final String FRIEND_LIST = "friendList";
    public static final String USER_LIST = "userList";
    public static final String USER_CODE = "userCode";
    public static final String USER_INFO = "userInfo";
    public static final String PREVIEW_PATH = "previewPath";
    public static final String DOWNLOAD_PATH = "downloadPath";
    public static final String FILE_TYPE = "fileType";
    public static final String USER_PIC = "userPic";
    public static final String ROOM_ID = "roomId";
    public static final String ROOM_NAME = "roomName";
    public static final String IS_GROUP = "isGroup";
    public static final String ROOM_PIC = "roomPic";
    public static final String ALBUM = "album";
}
