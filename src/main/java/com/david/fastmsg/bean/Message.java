package com.david.fastmsg.bean;

/**
 * @author qzw
 * @date 2021/9/3
 */
public class Message {
    /**
     * 消息id
     */
    private String id;

    /**
     * 发送者
     */
    private String send;

    /**
     * 发送者userId
     */
    private String sendUserId;

    /**
     * 接收者
     */
    private String receive;

    /**
     * 接受者userId
     */
    private String receiveUserId;

    /**
     * 文件类型
     * 0 文字 1 图片 2 视频文件 3 一般文件
     */
    private String fileType;

    /**
     * 内容
     */
    private String info;

    /**
     * 消息类型 0 下线 1 上线 2 私人聊天 3 好友申请 4 群上线 5 群聊天
     */
    private int type;

    /**
     * 展示图片标签 只有普通上线与群聊上线才会将头像信息传输
     */
    private String pic;

    public Message(String id, String send, String sendUserId, String receive, String receiveUserId, String info, String fileType, int type, String pic) {
        this.id = id;
        this.send = send;
        this.sendUserId = sendUserId;
        this.receive = receive;
        this.receiveUserId = receiveUserId;
        this.info = info;
        this.fileType = fileType;
        this.type = type;
        this.pic = pic;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSend() {
        return send;
    }

    public void setSend(String send) {
        this.send = send;
    }

    public String getReceive() {
        return receive;
    }

    public void setReceive(String receive) {
        this.receive = receive;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getSendUserId() {
        return sendUserId;
    }

    public void setSendUserId(String sendUserId) {
        this.sendUserId = sendUserId;
    }

    public String getReceiveUserId() {
        return receiveUserId;
    }

    public void setReceiveUserId(String receiveUserId) {
        this.receiveUserId = receiveUserId;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }
}
