package com.david.fastmsg.repository;

import com.david.fastmsg.modal.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;


@Repository
public interface UserRepository extends JpaRepository<Users, Integer> {

    Users findByNickNameAndPassword(String nickName, String password);

    Users getByUserId(String userId);

    Users getByNickName(String nickName);

    Users findByNickName(String nickName);

    @Query(value = "select * from t_user where nick_name like (%?1%)", nativeQuery = true)
    List<Users> findByNickNameExists(String nickName);

    @Query(value = "select userId as userId,userName as userName,picSmall as picSmall from Users where userCode = :userCode")
    Map<String, Object> findByUserCode(@Param("userCode") String userCode);

    Users findByUserId(String userId);

    @Query(value = "select * from t_user where user_id in (?1)", nativeQuery = true)
    List<Users> findAllByUserIdList(List<String> userIdList);

    @Modifying
    @Query(value = "update Users set picSmall = ?1 where userId = ?2")
    void updateUserPic(String userPic, String userId);


//    @Query(value = "select id as id,name as name,isEnhance as '%?1%' isEnhance from Template order by createdAt asc ")
//    List<Map<String,String>> findAllTemplate();
//
//    @Query(value = "select * from t_user where id = ?1", nativeQuery = true)
//    Users findByUserId(Integer userId);
}
