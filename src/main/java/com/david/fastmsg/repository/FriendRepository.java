package com.david.fastmsg.repository;

import com.david.fastmsg.modal.Friend;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface FriendRepository extends JpaRepository<Friend, Integer> {

    List<Friend> findByUserId(String userId);

    @Modifying
    @Query(value = "update Friend set friendPic = ?1 where friendUserId = ?2")
    void updateUserPicByFriendUserId(String userPic, String userId);
}
