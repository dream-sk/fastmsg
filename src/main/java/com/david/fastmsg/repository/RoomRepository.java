package com.david.fastmsg.repository;

import com.david.fastmsg.modal.Room;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface RoomRepository extends JpaRepository<Room, Integer> {

    Room findByRoomId(String receiveUserId);
}
