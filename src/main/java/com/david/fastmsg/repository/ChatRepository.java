package com.david.fastmsg.repository;

import com.david.fastmsg.modal.Chat;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface ChatRepository extends JpaRepository<Chat, Integer> {

    Chat findByRoomId(String receiveUserId);
}
