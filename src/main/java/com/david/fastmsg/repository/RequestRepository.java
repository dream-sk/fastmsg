package com.david.fastmsg.repository;

import com.david.fastmsg.modal.Friend;
import com.david.fastmsg.modal.Request;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface RequestRepository extends JpaRepository<Request, Integer> {

    List<Friend> findByUserId(String userId);

    Request findByUserIdAndFriendId(String userId, String friendUserId);
}
