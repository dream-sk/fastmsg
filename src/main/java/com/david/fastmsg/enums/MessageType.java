package com.david.fastmsg.enums;

/**
 * @author qzw
 * @date 2021/9/6
 */
public enum MessageType {

//    0 下线 1 上线 2 发消息 3 好友邀请
    OFFLINE(0),
    ONLINE(1),
    CHAT(2),
    FRIEND_REQUEST(3);

    private int type;

    MessageType(int type) {
        this.type = type;
    }

    public int getType() {
        return type;
    }
}
