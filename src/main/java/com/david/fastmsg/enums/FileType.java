package com.david.fastmsg.enums;

/**
 * @author qzw
 * @date 2021/9/6
 */
public enum FileType {

//    0 文字 1 图片 2 视频文件 3 一般文件
    FONT("font"),
    PIC("pic"),
    VIDEO("video"),
    FILE("file");

    private String type;

    FileType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }
}
