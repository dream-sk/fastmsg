/*
 *  CMB Confidential
 *
 *  Copyright (C) 2018 China Merchants Bank Co., Ltd. All rights reserved.
 *
 *  No part of this file may be reproduced or transmitted in any form or by any
 *  means, electronic, mechanical, photocopying, recording, or otherwise, without
 *  prior written permission of China Merchants Bank Co., Ltd.
 */

package com.david.fastmsg.enums;

import com.david.fastmsg.Constant.Param;
import com.david.fastmsg.Constant.RespCode;
import com.david.fastmsg.utils.JsonUtils;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * 返回话术规范:
 * <p>
 * 一、命名规范
 * 1、命名应遵循以下规范：[类型]_[出现地]_[具体名称]
 * 1、所有异常情况分为逻辑错误以及抛出异常两类，命名分别以ERR以及EXP开头
 * 2、根据异常情况出现地分为:
 * CTL(controller)、SVC(service)、REP(reposity)、UTL(util)、FRM(form)、OTR(other)
 * <p>
 * 二、话术规范
 * 1、所有返回话术后需拼接标识，以方便排障
 * 2、标识规范如下：[RS-XXXX]，XXXX为三位数字
 *
 */
public enum ResponseEnum {

    EXP_SVC_COMMON_ERROR(RespCode.CODE_1001, "失败[FM-0001]"),

    EXP_SVC_USER_LOGIN_FAIL(RespCode.CODE_1001, "用户登录失败[FM-0002]"),

    EXP_SVC_FRIEND_NOT_EXIST(RespCode.CODE_1001, "好友账号不存在[FM-0003]"),

    EXP_SVC_USER_EXISTS(RespCode.CODE_1001, "当前昵称已存在[FM-0004]"),

    EXP_SVC_USER_NOT_EXISTS(RespCode.CODE_1001, "用户查询不存在[FM-0005]"),

    ERR_SVC_UPLOAD_FILE_EXCEPTION(RespCode.CODE_1001, "文件上传失败[FM-0006]"),

    ERR_CTL_FILE_NOT_EXISTS(RespCode.CODE_1001, "文件不存在[FM-1001]"),

    ERR_CTL_FILE_PREVIEW_FAIL(RespCode.CODE_1001, "文件预览失败[FM-1002]"),

    ERR_CTL_FILE_DOWNLOAD_FAIL(RespCode.CODE_1001, "文件下载失败[FM-1003]"),
    ;

    private String respCode;

    public String getRespMsg() {
        return respMsg;
    }

    private String respMsg;


    ResponseEnum(String respCode, String respMsg) {
        this.respCode = respCode;
        this.respMsg = respMsg;
    }

    public String getRespCode() {
        return respCode;
    }


    /**
     * 将返回结果转换成JsonString
     *
     * @return String
     */
    public String toJsonString() {
        return JsonUtils.objectToJsonStringDisableHtmlEscaping(toMap());
    }

    /**
     * 将返回结果转换成Map
     *
     * @return Map
     */
    public Map<String, Object> toMap() {
        LinkedHashMap<String, Object> respMap = new LinkedHashMap<>();
        respMap.put(Param.RESP_CODE, respCode);
        respMap.put(Param.RESP_MSG, respMsg);
        return respMap;
    }
}
