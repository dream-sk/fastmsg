package com.david.fastmsg.form;

import javax.validation.constraints.NotEmpty;

public class AlbumForm {

    @NotEmpty(message = "userId不可为空", groups = {GET_ALBUM.class})
    private String userId;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }


    public interface GET_ALBUM {}

}
