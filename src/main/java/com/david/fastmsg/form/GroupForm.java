package com.david.fastmsg.form;

import javax.validation.constraints.NotEmpty;

public class GroupForm {

    private String groupName;

    @NotEmpty(message = "聊天列表不可为空", groups = {CREATE_GROUP.class})
    private String chatList;

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getChatList() {
        return chatList;
    }

    public void setChatList(String chatList) {
        this.chatList = chatList;
    }

    public interface CREATE_GROUP {}
}
