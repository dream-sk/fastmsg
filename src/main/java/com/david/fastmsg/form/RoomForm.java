package com.david.fastmsg.form;

import javax.validation.constraints.NotEmpty;

public class RoomForm {

    private String roomName;

    /**
     * 聊天列表
     */
    @NotEmpty(message = "chatList不可为空", groups = {RoomForm.CreateRoom.class})
    private String chatList;

    public String getRoomName() {
        return roomName;
    }

    public void setRoomName(String roomName) {
        this.roomName = roomName;
    }

    public String getChatList() {
        return chatList;
    }

    public void setChatList(String chatList) {
        this.chatList = chatList;
    }

    public interface CreateRoom {}
}
