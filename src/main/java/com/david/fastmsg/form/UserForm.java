package com.david.fastmsg.form;

import javax.validation.constraints.NotEmpty;

public class UserForm {

    @NotEmpty(message = "姓名不可为空")
    private String userName;

    @NotEmpty(message = "密码不可为空", groups = {CreateUser.class, Login.class})
    private String password;

    private String picSmall;

    private String picNormal;

    @NotEmpty(message = "登录名不可为空", groups = {CreateUser.class, Login.class})
    private String nickName;

    private String sign;

    private String phone;

    @NotEmpty(message = "userCode不可为空", groups = {FindByUserCode.class})
    private String userCode;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPicSmall() {
        return picSmall;
    }

    public void setPicSmall(String picSmall) {
        this.picSmall = picSmall;
    }

    public String getPicNormal() {
        return picNormal;
    }

    public void setPicNormal(String picNormal) {
        this.picNormal = picNormal;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getUserCode() {
        return userCode;
    }

    public void setUserCode(String userCode) {
        this.userCode = userCode;
    }

    public interface CreateUser {}

    public interface Login {}

    public interface FindByUserCode {}
}
