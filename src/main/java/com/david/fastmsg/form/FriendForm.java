package com.david.fastmsg.form;

import javax.validation.constraints.NotEmpty;

public class FriendForm {

    @NotEmpty(message = "userId不可为空", groups = {FRIEND_LIST.class, ADD_FRIEND.class, FRIEND_REQUEST.class})
    private String userId;

    @NotEmpty(message = "friendUserId不可为空", groups = {ADD_FRIEND.class,FRIEND_REQUEST.class})
    private String friendUserId;

    private String userCode;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getFriendUserId() {
        return friendUserId;
    }

    public void setFriendUserId(String friendUserId) {
        this.friendUserId = friendUserId;
    }

    public String getUserCode() {
        return userCode;
    }

    public void setUserCode(String userCode) {
        this.userCode = userCode;
    }

    public interface FRIEND_LIST {}

    public interface ADD_FRIEND {}

    public interface FRIEND_REQUEST {}
}
