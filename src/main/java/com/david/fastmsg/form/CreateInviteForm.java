package com.david.fastmsg.form;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class CreateInviteForm {

    @NotEmpty(message = "邀请人姓名不可为空")
    private String creatorName;

    @NotNull(message = "模板号不能为空")
    private Integer templateId;

    @NotEmpty(message = "isEnhance不能为空")
    private String isEnhance;

    public String getCreatorName() {
        return creatorName;
    }

    public void setCreatorName(String creatorName) {
        this.creatorName = creatorName;
    }

    public Integer getTemplateId() {
        return templateId;
    }

    public void setTemplateId(Integer templateId) {
        this.templateId = templateId;
    }

    public String getIsEnhance() {
        return isEnhance;
    }

    public void setIsEnhance(String isEnhance) {
        this.isEnhance = isEnhance;
    }
}
