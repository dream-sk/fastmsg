package com.david.fastmsg.controller;

import com.david.fastmsg.form.FriendForm;
import com.david.fastmsg.service.FriendService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

@Validated
@Controller
@RequestMapping(value = "/friend")
public class FriendController {


    @Autowired
    private FriendService friendService;

    /**
     * <pre>
     *
     * 接口功能： 申请添加好友
     *
     * 接口路径： /friend/friendRequest
     *
     * 接口描述： 向账号发出申请好友的申请，并且在数据库生成相应记录
     *
     * 输入参数：
     * friendUserId                   |String|M| 好友id
     * userId                         |String|M| userId
     *
     * 输出参数：
     * respCode                       |String|M| 返回码
     * respMsg                        |String|M| 返回描述
     * </pre>
     *
     * @param form
     * @return
     */
    @RequestMapping( value = "/friendRequest", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> friendRequest(@Validated(value = FriendForm.FRIEND_REQUEST.class) FriendForm form) {
        return friendService.friendRequest(form.getUserId(), form.getFriendUserId());
    }

    /**
     * <pre>
     *
     * 接口功能： 获取所有好友
     *
     * 接口路径： /friend/getFriendList
     *
     * 接口描述： 从数据库查询所有的好友
     *
     * 输入参数：
     * userId                         |String|M| userId
     *
     * 输出参数：
     * respCode                       |String|M| 返回码
     * respMsg                        |String|M| 返回描述
     * friendList                     |List
     *   userId                       |String|M| userId
     *   friendUserId                 |String|M| 好友userId
     *   friendName                   |String|M| 好友姓名
     *   friendPic                    |String|M| 好友头像
     *
     * </pre>
     *
     * @param form
     * @return
     */
    @RequestMapping( value = "/getFriendList", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> getFriendList(@Validated(value = FriendForm.FRIEND_LIST.class) FriendForm form) {
        return friendService.getFriendList(form.getUserId());
    }


    /**
     * <pre>
     *
     * 接口功能： 添加好友
     *
     * 接口路径： /friend/addFriend
     *
     * 接口描述： 同意好友申请，并且落库
     *
     * 输入参数：
     * userId                         |String|M| userId
     * friendUserId                   |String|M| 好友id
     *
     * 输出参数：
     * respCode                       |String|M| 返回码
     * respMsg                        |String|M| 返回描述
     *
     * </pre>
     *
     * @param form
     * @return
     */
    @RequestMapping( value = "/addFriend", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> addFriend(@Validated(value = FriendForm.ADD_FRIEND.class) FriendForm form) {
        return friendService.addFriend(form.getUserId(), form.getFriendUserId());
    }

}
