package com.david.fastmsg.controller;

import com.david.fastmsg.form.FriendForm;
import com.david.fastmsg.form.GroupForm;
import com.david.fastmsg.service.FriendService;
import com.david.fastmsg.service.GroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

@Validated
@Controller
@RequestMapping(value = "/group")
public class GroupController {


    @Autowired
    private GroupService groupService;

    /**
     * <pre>
     *
     * 接口功能： 创建群聊
     *
     * 接口路径： /group/create
     *
     * 接口描述： 创建群，没有群名称，会使用好友的名字拼接，聊天列表内的所有好友都会收到加群消息
     *
     * 输入参数：
     * groupName                      |String|O| 群名称
     * chatList                       |JSONString|M| 聊天列表
     *
     * 输出参数：
     * respCode                       |String|M| 返回码
     * respMsg                        |String|M| 返回描述
     * roomId                         |String|M| 房间编号
     * isGroup                        |String|M| 是否是群
     * roomName                       |String|M| 房间名称
     *
     * </pre>
     * @param form
     * @return
     */
    @RequestMapping( value = "/create", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> addFriend(@Validated(value = GroupForm.CREATE_GROUP.class) GroupForm form) {
        return groupService.createGroup(form.getGroupName(), form.getChatList());
    }

}
