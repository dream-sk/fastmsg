package com.david.fastmsg.controller;

import cn.hutool.core.codec.Base32;
import com.david.fastmsg.Constant.Param;
import com.david.fastmsg.enums.ResponseEnum;
import com.david.fastmsg.exception.RespException;
import com.david.fastmsg.form.UserForm;
import com.david.fastmsg.service.FileService;
import com.david.fastmsg.service.UsersService;
import com.david.fastmsg.utils.ResponseUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import java.util.Map;

@Validated
@Controller
@RequestMapping(value = "/users")
public class UserController {

    @Autowired
    private UsersService usersService;

    @Autowired
    private FileService fileService;

    /**
     * <pre>
     *
     * 接口功能： 创建用户
     *
     * 接口路径： /users/create
     *
     * 接口描述： 用户注册
     *
     * 输入参数：
     * userName                      |String|M| 用户名
     * password                      |String|M| 密码
     * nickName                      |String|M| 登录名称
     *
     * 输出参数：
     * respCode                       |String|M| 返回码
     * respMsg                        |String|M| 返回描述
     * url                            |String|M| 用户信息userId_userName_userCode_userPic

     * </pre>
     *
     * @param form
     * @return
     */
    @RequestMapping(value = "/create", method = RequestMethod.POST)
    @ResponseBody
    public Map<String,Object> createInvite(@Validated(value = UserForm.CreateUser.class) UserForm form) {
        String password = Base32.encode(form.getPassword());
        return usersService.createUser(form.getUserName(), password, form.getNickName());
    }


    /**
     * <pre>
     *
     * 接口功能： 登录
     *
     * 接口路径： /users/login
     *
     * 接口描述： 用户注册
     *
     * 输入参数：
     * password                      |String|M| 密码
     * nickName                      |String|M| 登录名称
     *
     * 输出参数：
     * respCode                       |String|M| 返回码
     * respMsg                        |String|M| 返回描述
     * url                            |String|M| 用户信息userId_userName_userCode_userPic

     * </pre>
     * @param form
     * @return
     */
    @RequestMapping( value = "/login", method = RequestMethod.POST)
    @ResponseBody
    public Map<String,Object> login(@Validated(value = UserForm.Login.class) UserForm form) {
        String password = Base32.encode(form.getPassword());
        return usersService.login(form.getNickName(), password);
    }

    /**
     * <pre>
     *
     * 接口功能： 查找联系人
     *
     * 接口路径： /users/findByUserCode
     *
     * 接口描述： 用户注册
     *
     * 输入参数：
     * userCode                      |String|M| 用户账号
     *
     * 输出参数：
     * respCode                       |String|M| 返回码
     * respMsg                        |String|M| 返回描述
     * userInfo                       |JSON|M|  用户信息
     * </pre>
     *
     * @param form
     * @return
     */
    @RequestMapping( value = "/findByUserCode", method = RequestMethod.POST)
    @ResponseBody
    public Map<String,Object> findByNickName(@Validated(value = UserForm.FindByUserCode.class) UserForm form) {
        return usersService.findByUserCode(form.getUserCode());
    }


    /**
     * <pre>
     *
     * 接口功能： 更新头像接口
     *
     * 接口路径： /users/updateHead
     *
     * 接口描述： 根据userId更新用户头像
     *
     * 输入参数：
     * file                      |MultipartFile|M| 文件
     * userId                   |String|M| userId
     *
     * 输出参数：
     * respCode                       |String|M| 返回码
     * respMsg                        |String|M| 返回描述
     * previewPath                    |String|O| 图片预览地址
     * downloadPath                   |String|O| 文件下载地址
     * fileType                       |String|M| 文件类型， pic|other
     * </pre>
     * @param file
     * @return
     */
    @RequestMapping(value = "/updateHead", method = RequestMethod.POST)
    @ResponseBody
    public Map upload(@RequestParam("file") MultipartFile file, @RequestParam("userId") String userId) {
        if (file.isEmpty()) {
            throw new RespException(ResponseEnum.ERR_CTL_FILE_NOT_EXISTS);
        }
        Map<String, Object> result = fileService.upload(file);
        result.putAll(usersService.updateUserHead(userId, (String)result.get(Param.PREVIEW_PATH)));
        return result;
    }

}
