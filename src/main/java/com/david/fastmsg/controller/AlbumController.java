package com.david.fastmsg.controller;

import com.david.fastmsg.Constant.Param;
import com.david.fastmsg.enums.ResponseEnum;
import com.david.fastmsg.exception.RespException;
import com.david.fastmsg.form.AlbumForm;
import com.david.fastmsg.service.AlbumService;
import com.david.fastmsg.service.FileService;
import com.david.fastmsg.utils.ResponseUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import java.util.Map;

@Validated
@Controller
@RequestMapping(value = "/album")
public class AlbumController {

    @Autowired
    private AlbumService albumService;

    @Autowired
    private FileService fileService;

    @RequestMapping(value = "/uploadAlbum", method = RequestMethod.POST)
    @ResponseBody
    public Map upload(@RequestParam("files") MultipartFile[] files, @RequestParam("userId") String userId) {
        if (files == null || files.length <= 0) {
            throw new RespException(ResponseEnum.ERR_CTL_FILE_NOT_EXISTS);
        }
        for (MultipartFile file: files) {
            Map<String, Object> upload = fileService.upload(file);
            albumService.uploadAlbum(userId, (String)upload.get(Param.PREVIEW_PATH));
        }
        return ResponseUtils.success();
    }



    @RequestMapping(value = "/getAlbum", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> getAlbum(@Validated(value = AlbumForm.GET_ALBUM.class) AlbumForm form) {
        return albumService.getAlbum(form.getUserId());
    }
}
