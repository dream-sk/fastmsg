package com.david.fastmsg.modal;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

/**
 * @author qzw
 * @date 2021/9/2
 */
@Entity
@Table(name = "t_room")
public class Room {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private String id;

    private String roomId;
    // 所有人名字的结合
    private String roomName;

    private String roomPicSmall;

    private String roomPicBig;

    // 修改的别名
    private String roomNickName;

    private Date createTime;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getRoomName() {
        return roomName;
    }

    public void setRoomName(String roomName) {
        this.roomName = roomName;
    }

    public String getRoomPicSmall() {
        return roomPicSmall;
    }

    public void setRoomPicSmall(String roomPicSmall) {
        this.roomPicSmall = roomPicSmall;
    }

    public String getRoomPicBig() {
        return roomPicBig;
    }

    public void setRoomPicBig(String roomPicBig) {
        this.roomPicBig = roomPicBig;
    }

    public String getRoomNickName() {
        return roomNickName;
    }

    public void setRoomNickName(String roomNickName) {
        this.roomNickName = roomNickName;
    }

    public String getRoomId() {
        return roomId;
    }

    public void setRoomId(String roomId) {
        this.roomId = roomId;
    }
}
