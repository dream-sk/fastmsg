package com.david.fastmsg.utils;

import com.google.gson.*;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @author qzw
 * @date 2021/9/3
 */
public class JsonUtils {

    public static final String DATE_FORMAT_YYYYMMDDHHMMSS = "yyyy-MM-dd HH:mm:ss";
    private static final String TIMEZONE_BEIJING = "GMT+08:00";
    private static final String TIMESTAMP_FORMAT_YYYY_MM_DD_HH_MM_SS_SSS = "yyyy-MM-dd HH:mm:ss.SSS";
    private static final ThreadLocal<SimpleDateFormat> TIMESTAMP_FORMAT = new ThreadLocal<SimpleDateFormat>() {
        protected SimpleDateFormat initialValue() {
            return JsonUtils.getTimestampFormat();
        }
    };

    public JsonUtils() {
    }

    public static JsonObject stringToJsonObject(String string) {
        return (new JsonParser()).parse(string).getAsJsonObject();
    }

    public static <T> T stringToClassOfT(String string, Class<T> classOfT) {
        return createGson().fromJson(string, classOfT);
    }

    public static <T> T stringToClassOfTDefaultDateFormat(String string, Class<T> classOfT) {
        return createGsonDefaultDateFormat().fromJson(string, classOfT);
    }

    public static <T> T stringToClassOfTSetDateFormat(String string, Class<T> classOfT, String dateFormat) {
        return createGsonSetDateFormat(dateFormat).fromJson(string, classOfT);
    }

    public static <T> T stringToTypeOfT(String string, Type typeOfT) {
        return createGson().fromJson(string, typeOfT);
    }

    public static <T> T stringToTypeOfTDefaultDateFormat(String string, Type typeOfT) {
        return createGsonDefaultDateFormat().fromJson(string, typeOfT);
    }

    public static <T> T stringToTypeOfTSetDateFormat(String string, Type typeOfT, String dateFormat) {
        return createGsonSetDateFormat(dateFormat).fromJson(string, typeOfT);
    }

    public static <T> List<T> stringToList(String string) {
        return (List)stringToTypeOfT(string, (new TypeToken<List<T>>() {
        }).getType());
    }

    public static <K, V> Map<K, V> stringToMap(String string) {
        return (Map)stringToTypeOfT(string, (new TypeToken<Map<K, V>>() {
        }).getType());
    }

    public static <T> T jsonElementToTypeOfT(JsonElement json, Type typeOfT) {
        return createGson().fromJson(json, typeOfT);
    }

    public static <T> List<T> jsonElementToList(JsonElement json) {
        return (List)jsonElementToTypeOfT(json, (new TypeToken<List<T>>() {
        }).getType());
    }

    public static <K, V> Map<K, V> jsonElementToMap(JsonElement json) {
        return (Map)jsonElementToTypeOfT(json, (new TypeToken<Map<K, V>>() {
        }).getType());
    }

    public static JsonElement objectToJsonElement(Object obj) {
        return createGson().toJsonTree(obj);
    }

    public static JsonElement objectToJsonElement(Object obj, Type typeOfT) {
        return createGson().toJsonTree(obj, typeOfT);
    }

    public static String objectToJsonString(Object obj) {
        return createGsonSetDateFormat("yyyy-MM-dd HH:mm:ss").toJson(obj);
    }

    public static String objectToJsonStringSetDateFormat(Object obj, String dateFormat) {
        return objectToJsonString(obj, (new GsonBuilder()).setDateFormat(dateFormat));
    }

    public static String objectToJsonStringDisableHtmlEscaping(Object obj) {
        return objectToJsonString(obj, (new GsonBuilder()).disableHtmlEscaping());
    }

    public static String objectToJsonString(Object obj, GsonBuilder gsonBuilder) {
        return gsonBuilder.create().toJson(obj);
    }

    public static String getAsString(JsonObject jo, String name) {
        JsonElement je = jo.get(name);
        return je != null && !je.isJsonNull() ? je.getAsString() : null;
    }

    public static Timestamp getAsTimestamp(JsonObject jo, String name) throws ParseException {
        String str = getAsString(jo, name);
        return str == null ? null : stringToTimestamp(str);
    }

    private static SimpleDateFormat getTimestampFormat() {
        TimeZone timeZoneBeijing = TimeZone.getTimeZone("GMT+08:00");
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        format.setTimeZone(timeZoneBeijing);
        return format;
    }

    public static Timestamp stringToTimestamp(String str) throws ParseException {
        return new Timestamp(((SimpleDateFormat)TIMESTAMP_FORMAT.get()).parse(str).getTime());
    }

    public static Integer getAsInt(JsonObject jo, String name) {
        JsonElement je = jo.get(name);
        return je != null && !je.isJsonNull() ? je.getAsInt() : null;
    }

    public static Long getAsLong(JsonObject jo, String name) {
        JsonElement je = jo.get(name);
        return je != null && !je.isJsonNull() ? je.getAsLong() : null;
    }

    public static Boolean getAsBoolean(JsonObject jo, String name) {
        JsonElement je = jo.get(name);
        return je != null && !je.isJsonNull() ? je.getAsBoolean() : null;
    }

    public static JsonObject getAsJsonObject(JsonObject jo, String name) {
        JsonElement je = jo.get(name);
        return je != null && !je.isJsonNull() ? je.getAsJsonObject() : null;
    }

    public static String[] getAsStringArray(JsonObject jo, String name) {
        JsonElement je = jo.get(name);
        if (je != null && !je.isJsonNull()) {
            JsonArray ja = je.getAsJsonArray();
            if (ja.size() != 0) {
                String[] strs = new String[ja.size()];

                for(int i = 0; i < ja.size(); ++i) {
                    strs[i] = ja.get(i).getAsString();
                }

                return strs;
            }
        }

        return null;
    }

    private static Gson createGson() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(Date.class, new GsonDateDeserializer());
        return gsonBuilder.create();
    }

    private static Gson createGsonDefaultDateFormat() {
        return (new GsonBuilder()).setDateFormat("yyyy-MM-dd HH:mm:ss").create();
    }

    private static Gson createGsonSetDateFormat(String dateFormat) {
        return (new GsonBuilder()).setDateFormat(dateFormat).create();
    }


    /**
     * 将string转为map
     *
     * @param json
     * @return
     */
    public static Map<String, Object> string2Map(String json) {
        return jsonObjectToMap(stringToJsonObject(json));
    }

    /**
     * 将map转为String
     * 解决null未被序列化的问题
     *
     * @param map
     * @return
     */
    public static String map2String(Map<String, Object> map) {
        Gson gson = new GsonBuilder().serializeNulls().disableHtmlEscaping().create();
        return gson.toJson(map);
    }

    /**
     * 将JsonObject转为map
     *
     * @param json
     * @return
     */
    public static Map<String, Object> jsonObjectToMap(JsonObject json) {
        Map<String, Object> map = new LinkedHashMap<String, Object>();
        Set<Map.Entry<String, JsonElement>> entrySet = json.entrySet();
        for (Map.Entry<String, JsonElement> entry : entrySet) {
            String key = entry.getKey();
            Object value = entry.getValue();
            if (value instanceof JsonPrimitive) {
                String tmpValue = value.toString();
                if (((JsonPrimitive) value).isString()) {
                    // String
                    map.put(key, ((JsonPrimitive) value).getAsString());
                } else if (((JsonPrimitive) value).isBoolean()) {
                    // boolean
                    map.put(key, ((JsonPrimitive) value).getAsBoolean());
                }else if (((JsonPrimitive) value).isNumber()) {
                    // int
                    map.put(key, ((JsonPrimitive) value).getAsInt());
                } else if (tmpValue.contains(".")) {
                    // double
                    map.put(key, ((JsonPrimitive) value).getAsDouble());
                } else {
                    // long
                    map.put(key, ((JsonPrimitive) value).getAsLong());
                }
            } else if (value instanceof JsonArray) {
                map.put(key, jsonArrayToList((JsonArray) value));
            } else if (value instanceof JsonObject) {
                map.put(key, jsonObjectToMap((JsonObject) value));
            } else if (value instanceof JsonNull) {
                map.put(key, null);
            } else {
                map.put(key, value);
            }
        }
        return map;
    }

    /**
     * 将JsonArray转为list
     * @param json
     * @return
     */
    public static List<Object> jsonArrayToList(JsonArray json) {
        List<Object> list = new ArrayList<Object>();
        int size = json.size();
        for (int i = 0; i < size; i ++) {
            Object value = json.get(i);

            if (value instanceof JsonPrimitive) {
                String tmpValue = value.toString();
                if (((JsonPrimitive) value).isString()) {
                    // String
                    list.add(((JsonPrimitive) value).getAsString());
                } else if (((JsonPrimitive) value).isBoolean()) {
                    // boolean
                    list.add(((JsonPrimitive) value).getAsBoolean());
                } else if (((JsonPrimitive) value).isNumber()) {
                    // int
                    list.add(((JsonPrimitive) value).getAsInt());
                } else if (tmpValue.contains(".")) {
                    // double
                    list.add(((JsonPrimitive) value).getAsDouble());
                } else {
                    // long
                    list.add(((JsonPrimitive) value).getAsLong());
                }
            } else if(value instanceof JsonArray) {
                list.add(jsonArrayToList((JsonArray) value));
            } else if(value instanceof JsonObject) {
                list.add(jsonObjectToMap((JsonObject) value));
            } else if (value instanceof JsonNull) {
                list.add(null);
            } else {
                list.add(value);
            }
        }
        return list;
    }
}
