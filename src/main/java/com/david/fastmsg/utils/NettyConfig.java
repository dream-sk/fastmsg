package com.david.fastmsg.utils;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @author qzw
 * @date 2021/9/3
 */
@Component
public class NettyConfig {

    @Value("${netty.websocket.port}")
    private int port;//netty监听的端口

    @Value("${netty.websocket.path}")
    private String path;//websocket访问路径

    public long getMaxFrameSize() {
        return maxFrameSize;
    }

    public void setMaxFrameSize(long maxFrameSize) {
        this.maxFrameSize = maxFrameSize;
    }

    @Value("${netty.websocket.max-frame-size}")
    private long maxFrameSize;

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }


}
