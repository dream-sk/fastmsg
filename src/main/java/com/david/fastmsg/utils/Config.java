package com.david.fastmsg.utils;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class Config {

    @Value("${upload.fileSavePath.picture}")
    private String picturePath;

    @Value("${upload.fileSavePath.video}")
    private String videoPath;

    @Value("${upload.fileSavePath.file}")
    private String filePath;

    @Value("${upload.fileSavePath.basePath}")
    private String basePath;

    @Value("${server.port}")
    private String serverPort;

    public String getFilePath() {
        return filePath;
    }

    public String getVideoPath() {
        return videoPath;
    }

    public String getPicturePath() {
        return picturePath;
    }

    public String getBasePath() {
        return basePath;
    }

    public String getServerPort() {
        return serverPort;
    }
}
