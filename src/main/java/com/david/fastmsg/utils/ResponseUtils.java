package com.david.fastmsg.utils;

import com.david.fastmsg.Constant.Constant;
import com.david.fastmsg.Constant.RespCode;

import java.util.HashMap;
import java.util.Map;

public class ResponseUtils {
    public ResponseUtils() {
    }

    public static Map<String, Object> success(Map<String, ?> content) {
        HashMap<String, Object> result = new HashMap();
        result.put("respCode", RespCode.CODE_1000);
        result.put("respMsg", "成功");
        if (content != null) {
            result.putAll(content);
        }

        return result;
    }

    public static Map<String, Object> success(Object content) {
        HashMap<String, Object> result = new HashMap();
        result.put("respCode", RespCode.CODE_1000);
        result.put("respMsg", "成功");
        if (content != null) {
            result.put("data", content);
        }

        return result;
    }

    public static Map<String, Object> success(String data) {
        HashMap<String, Object> result = new HashMap();
        result.put("respCode", RespCode.CODE_1000);
        result.put("respMsg", "成功");
        result.put("data", data);
        return result;
    }

    public static Map<String, Object> success() {
        return success((Map) null);
    }

    public static Map<String, Object> fail(String respCode, String respMsg) {
        return fail(respCode, respMsg, (Map) null);
    }

    public static Map<String, Object> fail(String respCode, String respMsg, Map<String, ?> content) {
        HashMap<String, Object> result = new HashMap();
        result.put("respCode", respCode);
        result.put("respMsg", respMsg);
        if (content != null) {
            result.putAll(content);
        }

        return result;
    }

    public boolean isSuccess(Map<String, Object> result) {
        return result != null && RespCode.CODE_1000.equals(result.get("respCode"));
    }

    public String getCode(Map<String, Object> result) {
        return (String) result.get("respCode");
    }

    public String getMessage(Map<String, Object> result) {
        return (String) result.get("respMsg");
    }
}
