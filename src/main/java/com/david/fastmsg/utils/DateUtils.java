package com.david.fastmsg.utils;

import java.util.Calendar;

public class DateUtils {

    public static String generateTime() {
        Calendar now = Calendar.getInstance();
        return now.get(Calendar.YEAR) + "年" + getMoth(now) + "月" + getDay(now) + "日";
    }

    private static String getMoth(Calendar now) {
        int month = now.get(Calendar.MONTH) + 1;
        if (String.valueOf(month).length()<2) {
            return "0" + month;
        }
        return String.valueOf(month);
    }

    private static String getDay(Calendar now) {
        int day = now.get(Calendar.DAY_OF_MONTH);
        if (String.valueOf(day).length()<2) {
            return "0" + day;
        }
        return String.valueOf(day);
    }

    public static String generateTime2() {
        Calendar now = Calendar.getInstance();
        return now.get(Calendar.YEAR) + "-" + getMoth(now) + "-" + getDay(now);
    }


}
