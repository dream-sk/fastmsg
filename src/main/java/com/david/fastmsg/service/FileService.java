package com.david.fastmsg.service;

import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.util.Map;

public interface FileService {

    Map<String, Object> upload(MultipartFile file);

    File preview(String fileName);
}
