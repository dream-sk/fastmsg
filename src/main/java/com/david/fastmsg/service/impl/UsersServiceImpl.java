package com.david.fastmsg.service.impl;

import cn.hutool.core.codec.Base32;
import cn.hutool.core.net.NetUtil;
import com.david.fastmsg.Constant.Constant;
import com.david.fastmsg.Constant.Param;
import com.david.fastmsg.enums.ResponseEnum;
import com.david.fastmsg.exception.RespException;
import com.david.fastmsg.modal.Users;
import com.david.fastmsg.repository.FriendRepository;
import com.david.fastmsg.repository.UserRepository;
import com.david.fastmsg.service.UsersService;
import com.david.fastmsg.utils.Config;
import com.david.fastmsg.utils.ResponseUtils;
import com.david.fastmsg.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author qzw
 * @date 2021/9/6
 */
@Service
public class UsersServiceImpl implements UsersService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private FriendRepository friendRepository;

    @Autowired
    private Config config;
    /**
     * 注册
     * @param userName
     * @param password
     * @param nickName
     * @return
     */
    @Override
    public Map<String, Object> createUser(String userName, String password, String nickName) {
        Users user = userRepository.findByNickName(nickName);
        if (user != null) {
            throw new RespException(ResponseEnum.EXP_SVC_USER_EXISTS);
        }
        user = new Users();
        user.setUserId(Utils.getUserId());
        user.setUserName(userName);
        user.setPassword(password);
        user.setNickName(nickName);
        String userPic = Constant.HTTP + NetUtil.getLocalhost().getHostAddress() + ":" + config.getServerPort() + Utils.generateHead(userName);
        user.setUserCode(Utils.getUserCode());
        user.setPicSmall(userPic);
        user.setCreateTime(new Date());
        Users saveUser = userRepository.save(user);
        Map<String, Object> resultMap = ResponseUtils.success();
        resultMap.put(Param.URL, Base32.encode(saveUser.getUserId()));
        return resultMap;
    }

    /**
     * 登录
     * @param nickName
     * @param password
     * @return
     */
    @Override
    public Map<String, Object> login(String nickName, String password) {
        Users user = userRepository.findByNickNameAndPassword(nickName, password);
        if(user == null) {
            throw new RespException(ResponseEnum.EXP_SVC_USER_LOGIN_FAIL);
        }
        Map<String, Object> resultMap = ResponseUtils.success();
        resultMap.put(Param.URL, Base32.encode(user.getUserId()));
        return resultMap;
    }

    @Override
    public Map<String, Object> findByNickName(String nickName) {
        List<Users> users = userRepository.findByNickNameExists(nickName);
        if (CollectionUtils.isEmpty(users)) {
            throw new RespException(ResponseEnum.EXP_SVC_USER_NOT_EXISTS);
        }
        Map<String, Object> resultMap = ResponseUtils.success();
        resultMap.put(Param.USER_LIST, users);
        return resultMap;
    }

    @Override
    public Map<String, Object> findByUserCode(String userCode) {
        Map<String, Object> users = userRepository.findByUserCode(userCode);
        if (CollectionUtils.isEmpty(users)) {
            throw new RespException(ResponseEnum.EXP_SVC_USER_NOT_EXISTS);
        }
        Map<String, Object> resultMap = ResponseUtils.success();
        resultMap.put(Param.USER_INFO, users);
        return resultMap;
    }

    @Override
    @Transactional
    public Map<String, Object> updateUserHead(String userId, String userPic) {
        userRepository.updateUserPic(userPic, userId);
        friendRepository.updateUserPicByFriendUserId(userPic, userId);
        return ResponseUtils.success();
    }

    @Override
    public Users findByUserId(String userId) {
        return userRepository.findByUserId(userId);
    }
}
