package com.david.fastmsg.service.impl;

import com.david.fastmsg.Constant.Param;
import com.david.fastmsg.modal.Chat;
import com.david.fastmsg.modal.Room;
import com.david.fastmsg.repository.ChatRepository;
import com.david.fastmsg.repository.RoomRepository;
import com.david.fastmsg.service.RoomService;
import com.david.fastmsg.utils.ResponseUtils;
import com.david.fastmsg.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.Map;

/**
 * @author qzw
 * @date 2021/9/6
 */
@Service
public class RoomServiceImpl implements RoomService {

    @Autowired
    private RoomRepository roomRepository;

    @Autowired
    private ChatRepository chatRepository;

    @Override
    @Transactional(isolation= Isolation.DEFAULT, readOnly=true)
    public Map<String, Object> createRoom(String roomName, String chatList) {
        Room room = new Room();
        String roomId = Utils.getRoomId();
        room.setRoomId(roomId);
        room.setRoomName(roomName);
//        room.setRoomPicBig();
        room.setRoomPicSmall("");
        room.setCreateTime(new Date());
        roomRepository.save(room);
        Chat chat = new Chat();
        chat.setChatList(chatList);
        chat.setIsGroup(1);
        chat.setRoomId(roomId);
        chat.setCreateTime(new Date());
        chatRepository.save(chat);
        Map<String, Object> success = ResponseUtils.success();
        success.put(Param.ROOM_ID, roomId);
        success.put(Param.ROOM_PIC, room.getRoomPicSmall());
        success.put(Param.ROOM_NAME, room.getRoomName());
        return success;
    }
}
