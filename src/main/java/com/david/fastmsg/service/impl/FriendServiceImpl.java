package com.david.fastmsg.service.impl;

import com.david.fastmsg.Constant.Constant;
import com.david.fastmsg.Constant.Param;
import com.david.fastmsg.enums.ResponseEnum;
import com.david.fastmsg.exception.RespException;
import com.david.fastmsg.modal.Friend;
import com.david.fastmsg.modal.Request;
import com.david.fastmsg.modal.Users;
import com.david.fastmsg.repository.FriendRepository;
import com.david.fastmsg.repository.RequestRepository;
import com.david.fastmsg.repository.UserRepository;
import com.david.fastmsg.service.FriendService;
import com.david.fastmsg.utils.ResponseUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author qzw
 * @date 2021/9/6
 */
@Service
public class FriendServiceImpl implements FriendService {

    @Autowired
    private FriendRepository friendRepository;
    
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RequestRepository requestRepository;

    @Override
    public Map<String, Object> getFriendList(String userId) {
        List<Friend> friendList = friendRepository.findByUserId(userId);
        Map<String, Object> result = ResponseUtils.success();
        result.put(Param.FRIEND_LIST, friendList);
        return result;
    }

    @Override
    @Transactional
    public Map<String, Object> addFriend(String userId, String friendUserId) {
        // 将对方当做自己好友存储
        Users friend = userRepository.getByUserId(friendUserId);
        Friend friendBean = new Friend();
        friendBean.setUserId(userId);
        friendBean.setFriendUserId(friendUserId);
        friendBean.setFriendName(friend.getUserName());
        friendBean.setCreateTime(new Date());
        friendBean.setFriendPic(friend.getPicSmall());
        friendRepository.save(friendBean);
        // 将自己当对方好友存储
        Users user = userRepository.getByUserId(userId);
        Friend friendOther = new Friend();
        friendOther.setUserId(friendUserId);
        friendOther.setFriendUserId(userId);
        friendOther.setFriendName(user.getUserName());
        friendOther.setFriendPic(user.getPicSmall());
        friendOther.setCreateTime(new Date());
        friendRepository.save(friendOther);
        return ResponseUtils.success();
    }

    @Override
    public Map<String, Object> friendRequest(String userId, String friendUserId) {
        Request friendRequest = requestRepository.findByUserIdAndFriendId(userId, friendUserId);
        if (friendRequest != null) {
            return ResponseUtils.success();
        }

        // 查询要加好友的信息
        Users friend = userRepository.findByUserId(friendUserId);
        if (friend == null) {
            throw new RespException(ResponseEnum.EXP_SVC_FRIEND_NOT_EXIST);
        }
        // 查询请求人自己的信息
        Users requestUser = userRepository.findByUserId(userId);
        Request request = new Request();
        // 请求人信息
        request.setUserId(requestUser.getUserId());
        request.setUserName(requestUser.getUserName());
        // 好友信息
        request.setFriendId(friend.getUserId());
        request.setFriendName(friend.getUserName());
        request.setStatus(Constant.FRIEND_REQUEST);
        request.setCreateTime(new Date());
        requestRepository.save(request);
        return ResponseUtils.success();
    }
}
