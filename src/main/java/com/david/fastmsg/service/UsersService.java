package com.david.fastmsg.service;

import com.david.fastmsg.modal.Users;

import java.util.Map;

/**
 * @author qzw
 * @date 2021/9/2
 */
public interface UsersService {

    Map<String, Object> createUser(String userName, String password, String nickName);

    Map<String, Object> login(String userName, String password);

    Map<String,Object> findByNickName(String nickName);

    Map<String,Object> findByUserCode(String userCode);

    Map<String, Object> updateUserHead(String userId, String userPic);

    Users findByUserId(String userId);
}
