package com.david.fastmsg.service;

import java.util.Map;

/**
 * @author qzw
 * @date 2021/9/2
 */
public interface GroupService {

    Map<String, Object> createGroup(String groupName, String chatList);
}
