package com.david.fastmsg.service;

import java.util.Map;

/**
 * @author qzw
 * @date 2021/9/2
 */
public interface FriendService {

    Map<String, Object> getFriendList(String userId);

    Map<String,Object> addFriend(String userId, String friendUserId);

    Map<String,Object> friendRequest(String userId, String friendUserId);
}
