package com.david.fastmsg.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * @author qzw
 * @date 2021/9/2
 */
@Service
public class DiscardService {

    private static final Logger LOGGER = LoggerFactory.getLogger(DiscardService.class);

    public void discard(String message) {
        LOGGER.info("接收消息："+ message);
    }
}
