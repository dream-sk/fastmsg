package com.david.fastmsg.service;

import java.util.Map;

/**
 * @author qzw
 * @date 2021/9/2
 */
public interface AlbumService {

    void uploadAlbum(String userId, String s);

    Map<String,Object> getAlbum(String userId);
}
