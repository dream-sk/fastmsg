package com.david.fastmsg.service;

import java.util.Map;

/**
 * @author qzw
 * @date 2021/9/2
 */
public interface RoomService {

    Map<String, Object> createRoom(String roomName, String chatList);

}
