package com.david.fastmsg.exception;

import com.david.fastmsg.enums.ResponseEnum;

import java.util.Map;

public class RespException extends RuntimeException {

    private String respCode;

    private String respMsg;

    private Map<String,Object> data;

    public RespException(String respCode, String respMsg, Map<String,Object> data) {
        this.respCode = respCode;
        this.respMsg = respMsg;
        this.data = data;
    }

    public RespException(ResponseEnum responseEnum) {
        this.respCode = responseEnum.getRespCode();
        this.respMsg = responseEnum.getRespMsg();
    }

    public RespException(String respCode, String respMsg) {
        this.respCode = respCode;
        this.respMsg = respMsg;
    }

    public String getRespCode() {
        return respCode;
    }

    public void setRespCode(String respCode) {
        this.respCode = respCode;
    }

    public String getRespMsg() {
        return respMsg;
    }

    public void setRespMsg(String respMsg) {
        this.respMsg = respMsg;
    }

    public Map<String, Object> getData() {
        return data;
    }

    public void setData(Map<String, Object> data) {
        this.data = data;
    }
}
