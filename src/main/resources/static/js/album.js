function clickAlbum() {
    $("#talkbody").hide();
    $("#talkbox").hide();
    $("#albumBody").show();
    $("#friend_nickName").text('');
    getAlbum();
}

function uploadAlbum() {
    var input = document.getElementById('uploadAlbum');
    input.click();
    input.onchange = function(){
        var object=this;
        var form = new FormData(); // FormData 对象
        for (var i = 0, len = object.files.length; i < len; i++) {
            form.append('files', object.files[i]);
        }
        // form.append("files", object.files);
        form.append("userId", userId);
        $.ajax({
            url: "/album/uploadAlbum",
            data: form,
            dataType: "json",
            type: "POST",
            contentType: false,
            processData: false,
            beforeSend: function (XHR) {
                $.AMUI.progress.start();
            },
            complete: function () {
                $.AMUI.progress.done();
            },
            success: function (result) {
                if (result.respCode == "1000") {
                    swal("恭喜!", result.respMsg, "success");
                    getAlbum();
                }
            },
            error: function (xmlHttpReq, error, ex) {
                swal("发生了一些问题", xmlHttpReq.responseJSON.error, "error");
            }
        });


    }
}

function getAlbum() {
    $.ajax({
        url: "/album/getAlbum",
        data: {
            userId: userId
        },
        type: "POST",
        beforeSend: function (XHR) {
            $.AMUI.progress.start();
        },
        complete: function () {
            $.AMUI.progress.done();
        },
        success: function (result) {
            if (result.respCode == "1000") {
                $(".show_album_page div").remove();
                if (result.album) {
                    for(var key in result.album) {
                        var userPicList = result.album[key];
                        var html = '<div class="album-page"><p>' + key + '</p><ul class="album_list">';
                        for (var i = 0; i < userPicList.length; i++) {
                            html += '<li><img src="' + userPicList[i] + '"/></li>';
                        }
                        html += '</ul></div>';
                        $(".show_album_page").append(html);
                        console.log('----------');
                    }
                } else {
                    var html = '<div class="album-page"><p>暂无</p><ul class="album_list"></ul></div>';
                    $(".show_album_page").append(html);
                }

            }
        },
        error: function (xmlHttpReq, error, ex) {
            swal("发生了一些问题", xmlHttpReq.responseJSON.error, "error");
        }
    });
}